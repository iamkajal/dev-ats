﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class UserGroup
    {
        [ForeignKey("UserId")]
        public int UserId { get; set; }
        //public virtual User User { get; set; }

        [ForeignKey("GroupId")]
        public int GroupId { get; set; }
        public virtual Group Group { get; set; }
    }
}
