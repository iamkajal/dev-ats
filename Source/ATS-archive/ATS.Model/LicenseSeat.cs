﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class LicenseSeat
    {
        public int Id { get; set; }

        [ForeignKey("LicenseId")]
        public int LicenseId { get; set; }
        public virtual License License { get; set; }

        [ForeignKey("AssignTo")]
        public int AssignTo { get; set; }
        // public virtual User User { get; set; }

        public string Note { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        // public virtual User User { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        [ForeignKey("AssetId")]
        public int AssetId { get; set; }
        public virtual Asset Asset { get; set; }
    }
}
