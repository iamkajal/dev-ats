﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class Departments
    {

        public int Id { get; set; }
        public string Name { get; set; }

        public int UserId { get; set; }
        //need to add

        [ForeignKey("CompanyId")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [ForeignKey("LocationId")]
        public int LocatonId { get; set; }
        public virtual Location Location { get; set; }

        [ForeignKey("ManagerId")]
        public int ManagerId { get; set; }
        // public virtual User User { get; set; }

        public string Note { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        public string Image { get; set; }

    }
}
