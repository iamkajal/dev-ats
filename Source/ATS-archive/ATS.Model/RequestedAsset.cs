﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class RequestedAsset
    {
        public int Id { get; set; }

        [ForeignKey("AssetId")]
        public int AssetId { get; set; }
        public virtual Asset Asset { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        // public virtual User User { get; set; }

        public DateTime AcceptedAt { get; set; }

        public DateTime DeniedAt { get; set; }

        public string Note { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
