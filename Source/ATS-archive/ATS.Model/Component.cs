﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class Component
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        [ForeignKey("LocationId")]
        public int LocatonId { get; set; }
        public virtual Location Location { get; set; }

        [ForeignKey("CompanyId")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public int Qty { get; set; }
        public string OderNumber { get; set; }

        public DateTime PurchaseDate { get; set; }

        public decimal PurchaseCost { get; set; }
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        public int MinmumAmt { get; set; }

        public string Serial { get; set; }

        public string Image { get; set; }

    }
}
