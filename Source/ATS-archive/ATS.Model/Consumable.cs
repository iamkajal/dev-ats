﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class Consumable
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey("CategoryId")]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        [ForeignKey("LocationId")]
        public int LocatonId { get; set; }
        public virtual Location Location { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        //public virtual User User { get; set; }


        public int Qty { get; set; }

        public bool Requestable { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        public DateTime PurchaseDate { get; set; }

        public decimal PurchaseCost { get; set; }

        public string OrderNumber { get; set; }

        [ForeignKey("CompanyId")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int MinmumAmt { get; set; }

        public string ModelNumber { get; set; }

        [ForeignKey("ManufacturerId")]
        public int ManufacturerId { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }

        public string ItemNo { get; set; }

        public string Image { get; set; }
    }
}
