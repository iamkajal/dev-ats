﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class AccessoriesUsers
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        //Need to add

        [ForeignKey("AccessoryId")]
        public int AccessoryId { get; set; }
        public virtual Accessory Accessory { get; set; }

        //Need to add
        public int AssignTo { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
