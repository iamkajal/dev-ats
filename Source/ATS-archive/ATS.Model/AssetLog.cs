﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class AssetLog
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        //Need to add

        public string ActionType { get; set; }

        public int AssetId { get; set; }

        //should be foreignkey
        public int CheckedoutTo { get; set; }

        [ForeignKey("LocationId")]
        public int LocatonId { get; set; }
        public virtual Location Location { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        public string Assetype { get; set; }
        public string Note { get; set; }

        public string FileName { get; set; }

        public DateTime RequestedAt { get; set; }

        public DateTime AcceptedAt { get; set; }

        public int accessory_id { get; set; }


        public int AcceptedId { get; set; }



        public int ConsumableId { get; set; }
        //Not clear



        public DateTime ExpectedCheckin { get; set; }

        public int CompnentId { get; set; }
    }
}
