﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class Supplier
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Address { get; set; }
        public string AddressTwo { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public string Country { get; set; }
        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Contact { get; set; }

        public string Notes { get; set; }

        public DateTime CreateAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        //public int UserId { get; set; }

        public DateTime DeletedAt { get; set; }

        public string Zip { get; set; }
        public string Url { get; set; }

        public string Image { get; set; }

        public virtual ICollection<Asset> Asset { get; set; }
    }
}
