﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ATS.Model
{
    public class Company
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
      
        public string Name { get; set; }



        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [Display(Name = "Image")]
        public string Image { get; set; }


        public virtual ICollection<Asset> Asset { get; set; }

        public virtual ICollection<Accessory> Accessory { get; set; }
        public virtual ICollection<Components> Component { get; set; }
        public virtual ICollection<Consumable> Consumable { get; set; }
    }
}
