﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class LicenseRepository : Repository<License>, ILicense
    {
        public LicenseRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
