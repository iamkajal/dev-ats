﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class ComponentAssetRepository : Repository<ComponentsAssets>, IComponentsAssets
    {
        public ComponentAssetRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
