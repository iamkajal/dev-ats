﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class ModelsRepository : Repository<Models>, IModels
    {
        public ModelsRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
