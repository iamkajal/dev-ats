﻿using ATS.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.Interface
{
    public interface ILocation : IRepository<Location>
    {
    }
}
