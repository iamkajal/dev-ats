﻿
using ATS.Model;
using Microsoft.EntityFrameworkCore;

namespace ATS.Context
{
    public class AtsDbContext : DbContext
    {
        public AtsDbContext(DbContextOptions<AtsDbContext> options) : base(options)
        {
        }

        public DbSet<Asset> Asset { get; set; }
        public DbSet<AccessoriesUsers> AccessoriesUsers { get; set; }

        public DbSet<Accessory> Accessory { get; set; }
        public DbSet<ActionLog> ActionLog { get; set; }

        public DbSet<AssetLog> AssetLog { get; set; }

        public DbSet<AssetMaintenance> AssetMaintenance { get; set; }

        public DbSet<AssetUploads> AssetUploads { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<CheckoutRequest> CheckoutRequest { get; set; }

        public DbSet<Company> Company { get; set; }

        public DbSet<Components> Component { get; set; }

        public DbSet<ComponentsAssets> ComponentsAssets { get; set; }

        public DbSet<Consumable> Consumable { get; set; }

        public DbSet<ConsumablesUsers> ConsumablesUsers { get; set; }

        public DbSet<Departments> Departments { get; set; }
        public DbSet<Depreciation> Depreciation { get; set; }

        public DbSet<Group> Group { get; set; }

        public DbSet<Import> Import { get; set; }

        public DbSet<License> License { get; set; }

        public DbSet<LicenseSeat> LicenseSeat { get; set; }

        public DbSet<Location> Location { get; set; }

        public DbSet<Manufacturer> Manufacturer { get; set; }

        public DbSet<Models> Models { get; set; }

        public DbSet<RequestedAsset> RequestedAsset { get; set; }

        public DbSet<Requests> Requests { get; set; }

        public DbSet<Status> Status { get; set; }
        public DbSet<StatusLabel> StatusLabel { get; set; }

        public DbSet<Supplier> Supplier { get; set; }

        public DbSet<Throttle> Throttle { get; set; }
        public DbSet<User> User { get; set; }

        public DbSet<UserGroup> UserGroup { get; set; }

    }
}
